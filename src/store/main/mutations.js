/*
export const someMutation = (state) => {}
 */

export const message = (state, payload) => {
  let { type, pkg } = payload;
  if (type === "UPDATE") {
    state.chats.push(pkg);
  }
  if (type === "LOAD") {
    state.chats = pkg;
  }
};

export const isLoggedIn = (state, payload) => {
  let { type, pkg } = payload;
  if (type === "UPDATE") {
    state.isLoggedIn = pkg;
  }
};
