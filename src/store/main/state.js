let isLoggedIn = localStorage.getItem("jwtToken") !== null ? true : false;

export default {
  chats: [],
  isLoggedIn: isLoggedIn,
};
