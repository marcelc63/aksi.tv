/*
export const someGetter = (state) => {}
 */

export const chats = (state) => {
  return state.chats;
};

export const isLoggedIn = (state) => {
  return state.isLoggedIn;
};
