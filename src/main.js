import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./main.css";
import "./style/global.scss";

import socket from "./plugins/socket.js";
socket({ Vue });

import chatscroll from "./plugins/chatscroll.js";
chatscroll({ Vue });

import modal from "./plugins/modal.js";
modal({ Vue });

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
