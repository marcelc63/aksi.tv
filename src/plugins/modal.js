import VModal from "vue-js-modal/dist/index.nocss.js";
import "vue-js-modal/dist/styles.css";

export default ({ Vue }) => {
  Vue.use(VModal);
};
