import VueSocketIO from "vue-socket.io";

console.log(process.env.VUE_APP_API);

const options = {};
if (![undefined, null].includes(localStorage.getItem("jwtToken"))) {
  options.query = "auth_token=" + localStorage.getItem("jwtToken");
}
export default ({ Vue }) => {
  Vue.use(
    new VueSocketIO({
      debug: true,
      connection: process.env.VUE_APP_API,
      options,
    })
  );
};
