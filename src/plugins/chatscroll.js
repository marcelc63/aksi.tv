import VueChatScroll from 'vue-chat-scroll'

export default ({ Vue }) => {
  Vue.use(VueChatScroll)
}
