//Dependencies
const express = require("express"),
  Session = require("express-session"),
  FileStore = require("session-file-store")(Session),
  app = express(),
  http = require("http").Server(app),
  port = 3011,
  path = require("path"),
  history = require("connect-history-api-fallback"),
  cors = require("cors"),
  flash = require("connect-flash"),
  node_media_server = require("./stream/media_server.js");

// Thumbnail Import
const thumbnail_generator = require("./cron/thumbnails");

//Express
var bodyParser = require("body-parser");

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  res.header("Access-Control-Allow-Methods", "*");
  next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(flash());
app.use(require("cookie-parser")());

var mongoose = require("mongoose");
var credentials = require("./config.js");
mongoose.Promise = require("bluebird");
mongoose
  .connect(credentials.mongoConnection, {
    promiseLibrary: require("bluebird"),
    useNewUrlParser: true,
    auth: { authdb: "admin" },
    useFindAndModify: false,
  })
  .then(() => console.log("connection succesful"))
  .catch((err) => console.error(err));
mongoose.set("useCreateIndex", true);

app.use(
  Session({
    store: new FileStore({
      path: "./server/sessions",
    }),
    secret: credentials.secret,
    maxAge: Date().now + 60 * 1000 * 30,
  })
);

// API
require("./sockets/index.js")(http);
require("./routes/index.js")(app);

//Load
//Basic
app.use(history());
app.use(express.static(__dirname + "/../dist"));

app.use("/thumbnails", express.static("./server/thumbnails"));

app.get("*", function(req, res) {
  res.sendFile(path.join(__dirname + "/../dist/index.html"));
});

//Initiate
http.listen(port, function() {
  console.log("listening on *:", port);
});

//Media Server
node_media_server.run();

//Cron Thumbnail
thumbnail_generator.start();
