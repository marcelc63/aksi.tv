let Chat = require("../models/Chat");

module.exports = function(io, socket) {
  let user = socket.request.user;

  socket.on("initiate", (packet) => {
    console.log("Join Roon");
    socket.join(packet.room);
  });

  socket.on("chat", (packet) => {
    if (!user.logged_in) {
      return;
    }

    //Save to database
    // let chat = new Chat({
    //   message: packet.message,
    //   user: user._id,
    //   username: user.username,
    //   room: packet.room,
    // });
    // chat.save();

    //Broadcast
    io.to(packet.room).emit("msg", {
      username: user.username,
      message: packet.message,
      datestamp: Date.now(),
    });
  });
};
