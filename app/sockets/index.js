var jwtAuth = require("socketio-jwt-auth");
var settings = require("../config/settings"); // get settings file
var User = require("../models/User");

module.exports = function(http) {
  //Define
  let io = require("socket.io")(http);

  //Handshake
  // using middleware
  io.use(
    jwtAuth.authenticate(
      {
        secret: settings.secret, // required, used to verify the token's signature
        algorithm: "HS256", // optional, default to be HS256
        succeedWithoutToken: true,
      },
      function(payload, done) {
        // console.log("handshake", payload);
        // you done callback will not include any payload data now
        // if no token was supplied
        if (payload && payload._id) {
          User.findOne(
            {
              _id: payload._id,
            },
            function(err, user) {
              if (err) {
                // return error
                return done(err);
              }
              if (!user) {
                // return fail with an error message
                return done(null, false, "user does not exist");
              }
              // return success with a user info
              return done(null, user);
            }
          );
        } else {
          return done();
        }
      }
    )
  );

  //Connection
  io.on("connection", function(socket) {
    require("./chat.js")(io, socket);
  });
};
