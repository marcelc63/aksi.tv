var express = require("express");
var router = express.Router();
var User = require("../models/User.js");
var Stream = require("../models/Stream.js");
var passport = require("passport");
require("../config/passport")(passport);
const shortid = require("shortid");

function getToken(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(" ");
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
}

router.get("/info", async function(req, res) {
  if (req.query.streams) {
    let streams = JSON.parse(req.query.streams);
    let query = { $or: [] };
    let match = [];
    for (let stream in streams) {
      if (!streams.hasOwnProperty(stream)) continue;
      query.$or.push({ stream_key: stream });
      match.push(stream);
    }

    let users = await User.aggregate([
      { $match: { stream_key: { $in: match } } },
      {
        $lookup: {
          from: "streams",
          localField: "_id",
          foreignField: "user",
          as: "streams",
        },
      },
    ]);

    //.find(query);

    let live = users.map((x) => {
      let [stream_info] = x.streams;
      return {
        stream_key: x.stream_key,
        username: x.username,
        title: stream_info.title,
        description: stream_info.description,
      };
    });

    // console.log(users);
    res.json(live);
  }
});

router.post("/user", async function(req, res) {
  let user = await User.findOne({ username: req.body.username });
  console.log(user);
  if (![undefined, null].includes(user)) {
    let stream = await Stream.findOne({ user: user._id });
    res.json({
      live: true,
      stream_key: user.stream_key,
      stream_info: {
        title: stream.title,
        description: stream.description,
      },
    });
    return;
  }
  res.json({ live: false });
});

router.get(
  "/stream_key",
  passport.authenticate("jwt", {
    session: false,
  }),
  async (req, res) => {
    //Authenticate
    if (!getToken(req.headers)) {
      return res.status(403).send({
        success: false,
        msg: "Unauthorized.",
      });
    }

    console.log("GET KEY", req.user);

    let user = await User.findOne({ username: req.user.username });
    console.log("RETRIEVE", user);

    if ([undefined, null, ""].includes(user.stream_key)) {
      user.stream_key = shortid.generate();
      user.save();
    }

    let stream = await Stream.findOne({ user: user._id });
    console.log("UPDATED", user);
    res.json({
      stream_key: user.stream_key,
      stream_info: {
        title: stream.title,
        description: stream.description,
      },
      username: user.username,
    });
  }
);

router.post(
  "/stream_key",
  passport.authenticate("jwt", {
    session: false,
  }),
  async (req, res) => {
    //Authenticate
    if (!getToken(req.headers)) {
      return res.status(403).send({
        success: false,
        msg: "Unauthorized.",
      });
    }

    let user = await User.findOneAndUpdate(
      {
        username: req.user.username,
      },
      {
        stream_key: shortid.generate(),
      },
      {
        upsert: true,
        new: true,
      }
    );

    res.json({
      stream_key: user.stream_key,
    });
  }
);

router.post(
  "/update_info",
  passport.authenticate("jwt", {
    session: false,
  }),
  async (req, res) => {
    //Authenticate
    if (!getToken(req.headers)) {
      return res.status(403).send({
        success: false,
        msg: "Unauthorized.",
      });
    }

    console.log("update info");

    let stream = await Stream.findOneAndUpdate(
      {
        user: req.user._id,
      },
      {
        title: req.body.title,
        description: req.body.description,
      }
    );

    res.json({
      success: "ok",
    });
  }
);

module.exports = router;
