module.exports = function(app) {
  //Routes
  app.use("/auth", require("./auth"));
  app.use("/streams", require("./streams"));
};
