var passport = require("passport");
var settings = require("../config/settings");
require("../config/passport")(passport);
var express = require("express");
var jwt = require("jsonwebtoken");
var router = express.Router();
const shortid = require("shortid");

var User = require("../models/User");
var Stream = require("../models/Stream");

router.get("/test", function(req, res) {
  res.send("test");
});

router.post("/register", async function(req, res) {
  console.log("register", res.body);
  if (!req.body.username || !req.body.password) {
    res.json({ success: false, msg: "Please pass username and password." });
  } else {
    try {
      let newUser = new User({
        username: req.body.username.toLowerCase(),
        email: req.body.email,
        password: req.body.password,
        stream_key: shortid.generate(),
      });
      // save the user
      await newUser.save();

      let newStream = new Stream({
        title: `${newUser.username}'s Stream`,
        description: `Welcome to ${newUser.username}'s Stream`,
        user: newUser._id,
      });
      await newStream.save();

      console.log("User Save Success");
      res.json({ success: true, msg: "Successful created new user." });
    } catch (err) {
      console.log(err);
      return res.json({ success: false, msg: "Username already exists." });
    }
  }
});

router.post("/login", function(req, res) {
  User.findOne(
    {
      username: req.body.username.toLowerCase(),
    },
    function(err, user) {
      if (err) throw err;

      if (!user) {
        res.status(401).send({
          success: false,
          msg: "Authentication failed. User not found.",
        });
      } else {
        // check if password matches
        user.comparePassword(req.body.password, function(err, isMatch) {
          if (isMatch && !err) {
            // if user is found and password is right create a token
            var token = jwt.sign(user.toJSON(), settings.secret);
            // return the information including token as JSON
            res.json({
              success: true,
              token: token,
            });
          } else {
            res.status(401).send({
              success: false,
              msg: "Authentication failed. Wrong password.",
            });
          }
        });
      }
    }
  );
});

router.post("/logout", function(req, res) {
  req.logout();
  res.json({ success: true });
});

module.exports = router;
