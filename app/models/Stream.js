var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

var StreamSchema = new Schema({
  title: String,
  description: String,
  live: {
    type: Boolean,
    default: false,
  },
  user: {
    type: ObjectId,
    ref: "User",
  },
});

module.exports = mongoose.model("Stream", StreamSchema);
