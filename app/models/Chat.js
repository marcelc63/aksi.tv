var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

var ChatSchema = new Schema({
  message: String,
  user: {
    type: ObjectId,
    ref: "User",
  },
  username: String,
  room: String,
  datestamp: {
    type: Date,
    // `Date.now()` returns the current unix timestamp as a number
    default: Date.now,
  },
});

module.exports = mongoose.model("Chat", ChatSchema);
