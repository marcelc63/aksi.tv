var Stream = require("../models/Stream.js");
const helpers = require("../helpers/helpers");

const NodeMediaServer = require("node-media-server"),
  config = require("../config/stream").rtmp_server,
  User = require("../models/User");

nms = new NodeMediaServer(config);

nms.on("prePublish", async (id, StreamPath, args) => {
  let stream_key = getStreamKeyFromStreamPath(StreamPath);
  console.log(
    "[NodeEvent on prePublish]",
    `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`
  );
  try {
    let user = await User.findOne({ stream_key: stream_key });
    if (!user) {
      console.log(`USER REJECTED: ${stream_key}`);
      let session = nms.getSession(id);
      session.reject();
    } else {
      // do stuff
      await Stream.findOneAndUpdate(
        {
          user: user._id,
        },
        {
          live: true,
        }
      );

      helpers.generateStreamThumbnail(stream_key);
      console.log(`USER CONNECTED: ${stream_key}`);
    }
  } catch (err) {
    console.log(`NO USER`);
    let session = nms.getSession(id);
    session.reject();
  }
});

nms.on("donePublish", async (id, StreamPath, args) => {
  let stream_key = getStreamKeyFromStreamPath(StreamPath);
  console.log(`CLOSE PUBLISH: ${stream_key}`);
  try {
    let user = await User.findOne({ stream_key: stream_key });
    if (!user) {
      console.log(`USER REJECTED: ${stream_key}`);
    } else {
      // do stuff
      await Stream.findOneAndUpdate(
        {
          user: user._id,
        },
        {
          live: false,
        }
      );
      console.log(`USER CONNECTED: ${stream_key}`);
    }
  } catch (err) {
    console.log(`NO USER`);
  }
});

const getStreamKeyFromStreamPath = (path) => {
  let parts = path.split("/");
  return parts[parts.length - 1];
};

module.exports = nms;
